#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "BACnet/bacnet.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_value(50)
{
    ui->setupUi(this);
    ui->spinBox->setValue(m_value);
    connect(ui->spinBox, SIGNAL(valueChanged(int)),this, SLOT(setValue(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setValue(int value)
{
    qDebug() << Q_FUNC_INFO;
    m_value = value;
    if(ui->spinBox->value()!=value) {
        ui->spinBox->setValue(value);
    }
    emit updatesFromUI(value);
}
