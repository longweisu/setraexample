#include <QDebug>
#include <QTimer>
#include <QThread>
#include "bacworker.h"



BACworker::BACworker(QObject *parent) : QObject(parent)
{
    qDebug() << Q_FUNC_INFO;
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(yield()));
    timer->start(2000);
}

void BACworker::init(){
}

void BACworker::yield(){
    qDebug() << Q_FUNC_INFO;
    externalfrMain();
}

void BACworker::setValue(int value){
    qDebug() << Q_FUNC_INFO << value;
}

void BACworker::externalfrMain(){
    qDebug() << Q_FUNC_INFO;
    frACallbacValue();
}


void BACworker::frACallbacValue(){
    qDebug() << Q_FUNC_INFO;
    setValue(1);
    emit valueChanged(1);

}
