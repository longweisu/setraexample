
#include <QDebug>
#include <QThread>
#include "bacworker.h"
#include "bacnet.h"

BACnet::BACnet(QObject *parent): QObject(parent)
{
    qDebug() << "BACnet Ctor";
    QThread* thread = new QThread();
    BACworker* worker = new BACworker();

    connect(thread,SIGNAL(started()),worker,SLOT(init()));
    connect(this,SIGNAL(updateFromUI(int)), worker, SLOT(setValue(int)));
    connect(worker, SIGNAL(valueChanged(int)), this, SIGNAL(updateFromNetwork(int)));
    worker->moveToThread(thread);
    thread->start();
}

//void BACnet::setValue(int value){
//    qDebug() << Q_FUNC_INFO;
//}

