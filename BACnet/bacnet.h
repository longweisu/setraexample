#ifndef BACNET_H
#define BACNET_H


#include <QObject>
#include "bacnet_global.h"

class BACNETSHARED_EXPORT BACnet : public QObject
{
    Q_OBJECT
public:
    explicit BACnet(QObject *parent = 0);
public slots:
signals:
    void updateFromNetwork(int value);
    void updateFromUI(int value);
};

#endif // BACNET_H
