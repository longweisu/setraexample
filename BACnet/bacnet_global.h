#ifndef BACNET_GLOBAL_H
#define BACNET_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(BACNET_LIBRARY)
#  define BACNETSHARED_EXPORT Q_DECL_EXPORT
#else
#  define BACNETSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // BACNET_GLOBAL_H
