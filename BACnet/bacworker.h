#ifndef BACWORKER_H
#define BACWORKER_H

#include <QObject>


class BACworker : public QObject
{
    Q_OBJECT
public:
    explicit BACworker(QObject *parent = 0);

public slots:
    void init();
    void yield();
    void setValue(int value);

signals:
    void valueChanged(int value);

private:
    void frMain();
    void frACallbacValue();//analog/binary/multistate {input, output}
    void frACallbackEvent();
    void frACallLoop();
    void frACallbackCOV();
};

#endif // BACWORKER_H
