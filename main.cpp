
#include <QApplication>
#include <QThread>
#include "mainwindow.h"
#include "BACnet/bacnet.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();



    BACnet bac;;
    QObject::connect(&w, SIGNAL(updatesFromUI(int)),&bac,SIGNAL(updateFromUI(int)));
    QObject::connect(&bac, SIGNAL(updateFromNetwork(int)), &w, SLOT(setValue(int)));

    return a.exec();
}
