#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void setValue(int value);

signals:
    void updatesFromUI(int newValue);

private:
    Ui::MainWindow *ui;
    int m_value;
};

#endif // MAINWINDOW_H
